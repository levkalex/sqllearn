﻿using SQL.Data.Interfaces;
using SQL.Data.Models.TaskDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = SQL.Data.Models.TaskDb.Task;

namespace SQL.Data.Database
{
    public class EFTaskRepository : ITaskRepository
    {
        TaskDbContext taskDbContext;

        public EFTaskRepository(TaskDbContext taskDbContext)
        {
            this.taskDbContext = taskDbContext;
        }

        public IQueryable<Task> Tasks => taskDbContext.Tasks;

        public IQueryable<Lesson> Lessons => taskDbContext.Lessons;
    }
}
