﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models.TaskDb
{
    public class Lesson
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }

        public ICollection<Task> Tasks { get; set; }
        public Lesson()
        {
            Tasks = new List<Task>();
        }
    }
}
