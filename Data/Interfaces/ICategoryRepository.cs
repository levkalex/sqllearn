﻿using SQL.Data.Database;
using SQL.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Interfaces
{
    public interface ICategoryRepository
    {
        IQueryable<Category> Categories { get; }
    }
}
