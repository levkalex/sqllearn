﻿using Microsoft.EntityFrameworkCore;
using SQL.Data.Interfaces;
using SQL.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Database
{
    public class EFUsStateRepository : IUsStatesRepository
    {
        private ApplicationDbContext context;

        public EFUsStateRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<UsState> UsStates => context.UsStates;
    }
}
