﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models
{
    public class Region
    {
        public int Id { get; set; }
        public string RegionDescription { get; set; }
    }
}
