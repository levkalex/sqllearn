﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }  
        public string Description { get; set; }
        public byte[] Picture { get; set; }
    }
}