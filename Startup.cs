﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SQL.Data.Database;
using SQL.Data.Interfaces;

namespace SQL
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
                Configuration["Data:SQL:ConnectionString"]));

            services.AddDbContext<TaskDbContext>(options => options.UseSqlServer(
               Configuration["Data:Task:ConnectionString"]));

            services.AddTransient<IContext, EFContext>();
            services.AddTransient<ICategoryRepository, EFCategoryRepository>();
            services.AddTransient<IUsStatesRepository, EFUsStateRepository>();
            services.AddTransient<ITaskRepository, EFTaskRepository>();

            services.AddMvc();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            //SeedData.EnsurePopulated(app);
        }
    }
}
