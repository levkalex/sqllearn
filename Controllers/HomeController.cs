﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SQL.Data.Interfaces;
using SQL.Data.Models;
using SQL.Data.ViewModels;

namespace SQL.Controllers
{
    public class HomeController : Controller
    {
        IContext context;
        IUsStatesRepository statesRepository;
        ITaskRepository taskRepository;

        public HomeController(IContext context, IUsStatesRepository statesRepository, ITaskRepository taskRepository)
        {
            this.context = context;
            this.statesRepository = statesRepository;
            this.taskRepository = taskRepository;
        }

        public IActionResult Index()
        {
            IEnumerable<IEntityType> types = context.Context.Model.GetEntityTypes();
            int countQuery = taskRepository.Tasks.Count();

            return View(new IndexViewModel { Types = types, CountQuery = countQuery });
        }


        public IActionResult Query(string sql = null, string task = null)
        {
            if (sql != null)
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();

                try
                {
                    var rawDate = context.Context.Database.ExecuteSqlQuery(sql);
                    watch.Stop();
                    var dt = new DataTable();
                    dt.Load(rawDate.DbDataReader);

                    ViewBag.Data = dt.AsEnumerable().ToList();
                    ViewBag.Columns = dt.AsEnumerable().ToList().FirstOrDefault().Table.Columns;
                    ViewBag.Time = watch.ElapsedMilliseconds;
                    ViewBag.Task = task;

                }
                catch (Exception e)
                {
                    ViewBag.Error = e.Message;
                }

                ViewBag.SQL = sql;
            }

            return View();
        }

        public IActionResult Tasks()
        {
            var lessons = taskRepository.Lessons.OrderBy(x=>x.Number).Include(x=>x.Tasks);
            
            return View(lessons);
        }
    }
}