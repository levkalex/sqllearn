﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models
{
    public class Territory
    {
        public int Id { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionId { get; set; }
    }
}
