﻿using SQL.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Database
{
    public class EFContext : IContext
    {
        public ApplicationDbContext Context { get; }
        public EFContext(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}
