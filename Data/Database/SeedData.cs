﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SQL.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL.Data.Database
{
    static class SeedData
    {
        static ApplicationDbContext context;

        public static void EnsurePopulated(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                ReloadCategories();
                ReloadUsStates();
                ReloadRegion();
                ReloadTerritories();
                ReloadSuppliers();
                ReloadShippers();
                ReloadProducts();
                ReloadOrders();
                ReloadOrderDetails();
                ReloadEmplyeeTerritories();
                ReloadCustomers();
                ReloadEmployees();
            }


        }

        public static void ReloadCategories()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Categories] ON");

            context.Categories.RemoveRange(context.Categories);
            context.SaveChanges();

            var data = GetData("categories.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Categories.Add(new Category { Id = Convert.ToInt32(mas[0]), CategoryName = mas[1], Description = mas[2], Picture = Encoding.ASCII.GetBytes(mas[3]) });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Categories] OFF");
        }
        public static void ReloadUsStates()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[UsStates] ON");

            context.UsStates.RemoveRange(context.UsStates);
            context.SaveChanges();

            var data = GetData("usStates.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.UsStates.Add(new UsState { Id = Convert.ToInt32(mas[0]), StateName = mas[1], StateAbbr = mas[2], StateRegion = mas[3] });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[UsStates] OFF");
        }

        public static void ReloadRegion()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Regions] ON");

            context.Regions.RemoveRange(context.Regions);
            context.SaveChanges();

            var data = GetData("region.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Regions.Add(new Region { Id = Convert.ToInt32(mas[0]), RegionDescription = mas[1] });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Regions] OFF");
        }


        public static void ReloadTerritories()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Territories] ON");

            context.Territories.RemoveRange(context.Territories);
            context.SaveChanges();

            var data = GetData("territories.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Territories.Add(new Territory { Id = Convert.ToInt32(mas[0]), TerritoryDescription = mas[1], RegionId = Convert.ToInt32(mas[2]) });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Territories] OFF");
        }


        public static void ReloadSuppliers()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Suppliers] ON");

            context.Suppliers.RemoveRange(context.Suppliers);
            context.SaveChanges();

            var data = GetData("suppliers.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Suppliers.Add(new Supplier
                {
                    Id = Convert.ToInt32(mas[0]),
                    CompanyName = mas[1],
                    ContactName = mas[2],
                    ContactTitle = mas[3],
                    Adress = mas[4],
                    City = mas[5],
                    Region = mas[6],
                    PostalCode = mas[7],
                    Country = mas[8],
                    Phone = mas[9],
                    Fax = mas[10],
                    HomePage = mas[11]
                });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Suppliers] OFF");
        }

        public static void ReloadShippers()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Shippers] ON");

            context.Shippers.RemoveRange(context.Shippers);
            context.SaveChanges();

            var data = GetData("shippers.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Shippers.Add(new Shipper
                {
                    Id = Convert.ToInt32(mas[0]),
                    CompanyName = mas[1],
                    Phone = mas[2]
                });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Shippers] OFF");
        }

        public static void ReloadProducts()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Products] ON");

            context.Products.RemoveRange(context.Products);
            context.SaveChanges();

            var data = GetData("products.csv");

            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Products.Add(new Product
                {
                    Id = Convert.ToInt32(mas[0]),
                    ProductName = mas[1],
                    SupplierId = Convert.ToInt32(mas[2]),
                    CategoryId = Convert.ToInt32(mas[3]),
                    QuantityPerUnit = mas[4],
                    UnitPrice = Convert.ToDecimal(mas[5], new CultureInfo("en-AU")),
                    UnitsInStock = Convert.ToInt32(mas[6]),
                    UnitsOnOrder = Convert.ToInt32(mas[7]),
                    ReorderLevel = Convert.ToInt32(mas[8]),
                    Discontinued = Convert.ToInt32(mas[9])
                });
            }

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Products] OFF");
        }

        public static void ReloadOrders()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Orders] ON");

            context.Orders.RemoveRange(context.Orders);
            context.SaveChanges();

            var data = GetData("orders.csv");
            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Orders.Add(new Order
                {
                    Id = Convert.ToInt32(mas[0]),
                    CustomerId = mas[1],
                    EmployeeId = Convert.ToInt32(mas[2]),
                    OrderDate = DateTime.ParseExact(mas[3], "yyyy-MM-dd", null),
                    RequiredDate = DateTime.ParseExact(mas[4], "yyyy-MM-dd", null),
                    ShippedDate = (CheckDateTimeParse(mas[5]) == true) ? DateTime.ParseExact(mas[5], "yyyy-MM-dd", null) : (DateTime?)null,
                    ShipVia = Convert.ToInt32(mas[6]),
                    Freight = Convert.ToDecimal(mas[7], new CultureInfo("en-AU")),
                    ShipName = mas[8],
                    ShipAddress = mas[9],
                    ShipCity = mas[10],
                    ShipRegion = (mas[11] != "\\N")?mas[11]:null,
                    ShipPostalCode = mas[12],
                    ShipCountry = mas[13]
                });
            }
           

            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Orders] OFF");
        }

        public static void ReloadOrderDetails()
        {
            context.OrderDetails.RemoveRange(context.OrderDetails);
            context.SaveChanges();

            var data = GetData("orderDetails.csv");
            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.OrderDetails.Add(new OrderDetail
                {
                    OrderId = Convert.ToInt32(mas[0]),
                    ProductId = Convert.ToInt32(mas[1]),
                    UnitPrice = Convert.ToDecimal(mas[2], new CultureInfo("en-AU")),
                    Quantity = Convert.ToInt32(mas[3]),
                    Discount = Convert.ToDecimal(mas[4], new CultureInfo("en-AU"))
                });
            }

            context.SaveChanges();
        }

        public static void ReloadEmplyeeTerritories()
        {
            context.EmployeeTerritories.RemoveRange(context.EmployeeTerritories);
            context.SaveChanges();

            var data = GetData("employeeTerritories.csv");
            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.EmployeeTerritories.Add(new EmployeeTerritory
                {
                    EmployeeId = Convert.ToInt32(mas[0]),
                    TerritoryId = Convert.ToInt32(mas[1])
                });
            }

            context.SaveChanges();
        }



        public static void ReloadCustomers()
        {
            //context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Customers] ON");
            context.Customers.RemoveRange(context.Customers);
            context.SaveChanges();

            var data = GetData("customers.csv");
            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Customers.Add(new Customer
                {
                    Id = mas[0],
                    CompanyName = mas[1],
                    ContactName = mas[2],
                    ContactTitle = mas[3],
                    Adress = mas[4],
                    City = mas[5],
                    Region = mas[6],
                    PostalCode = mas[7],
                    Country = mas[8],
                    Phone = mas[9],
                    Fax = mas[10]
                });
            }

            context.SaveChanges();
           // context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Customers] OFF");
        }


        public static void ReloadEmployees()
        {
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Employees] ON");

            context.Employees.RemoveRange(context.Employees);
            context.SaveChanges();

            var data = GetData("employees.csv");
            foreach (var line in data)
            {
                var mas = line.Split('|');

                context.Employees.Add(new Employee
                {
                    Id = Convert.ToInt32(mas[0]),
                    LastName = mas[1],
                    FirstName = mas[2],
                    Title = mas[3],
                    TitleOfCourtesy = mas[4],
                    BirthDate = DateTime.ParseExact(mas[5], "yyyy-MM-dd", null),
                    HireDate = DateTime.ParseExact(mas[6], "yyyy-MM-dd", null),
                    Adress = mas[7],
                    City = mas[8],
                    Region = mas[9],
                    PostalCode = mas[10],
                    Country = mas[11],
                    HomePhone = mas[12],
                    Extension = mas[13],
                    Photo = Encoding.ASCII.GetBytes(mas[14]),
                    Notes = mas[15],
                    ReportsTo = (mas[16]!= "\\N") ? Convert.ToInt32(mas[16]):(int?)null,
                    PhotoPath = mas[17]
                });
            }


            context.SaveChanges();
            context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[Employees] OFF");
        }


        public static bool CheckDateTimeParse(string date, string format = "yyyy-MM-dd")
        {
            DateTime temp;
            return DateTime.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out temp);
        }

        public static List<string> GetData(string file)
        {
            StreamReader sr = new StreamReader("Data/Seed/" + file);
            List<string> Data = new List<string>();

            while (!sr.EndOfStream)
            {
                Data.Add(sr.ReadLine());
            }

            return Data;
        }
    }

}
