﻿using Microsoft.EntityFrameworkCore;
using SQL.Data.Models.TaskDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = SQL.Data.Models.TaskDb.Task;

namespace SQL.Data.Database
{
    public class TaskDbContext : DbContext
    {
        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options) { }

        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lesson>().HasData(
                new Lesson[]
                {
                new Lesson { Id = 1, Number = 1, Name = "Простая фильтрация данных" },
                new Lesson { Id = 2, Number = 2, Name = "Использование операторов IN, DISTINCT, ORDER BY, NOT" },
                new Lesson { Id = 3, Number = 3, Name = "Использование оператора BETWEEN, DISTINCT" },
                new Lesson { Id = 4, Number = 4, Name = "Использование оператора LIKE" },
                new Lesson { Id = 5, Number = 5, Name = "Использование агрегатных функций (SUM, COUNT)" },
                new Lesson { Id = 6, Number = 6, Name = "Соединение таблиц, использование агрегатных функций и предложений GROUP BY и HAVING" },
                new Lesson { Id = 7, Number = 7, Name = "Использование JOIN" },
                new Lesson { Id = 8, Number = 8, Name = "Использование подзапросов" }
                });

            modelBuilder.Entity<Task>().HasData(
                new Task[]
                {
                    //les1
                new Task {
                    Id = 1,
                    LessonId = 1,
                    Number = 1,
                    TextAsk = "Выбрать в таблице <span>Orders</span> заказы, которые были сделаны после 16 августа 2017 года (колонка <span>OrderDate</span>) включительно и которые доставлены с <span>ShipVia</span> = 3. Запрос должен возвращать только колонки <span>OrderId</span>, <span>CustomerId</span>, <span>OrderDate</span>, и <span>ShipVia</span>.",
                    Answer = "select id, customerid, orderdate, shipvia"+Environment.NewLine +"from orders"+Environment.NewLine+"where orderdate >= '2017-08-16' and shipvia = 3"
                },
                 new Task {
                    Id = 2,
                    LessonId = 1,
                    Number = 2,
                    TextAsk = "Написать запрос, который выводит заказы без указанного <span>shipregion</span> из таблицы <span>orders</span>. В результатах запроса возвращать для колонки <span>shipregion</span> вместо значений NULL строку  ‘Not set’ (использовать системную функцию CASЕ).Запрос должен возвращать только колонки <span>orderid</span>, <span>orderdate</span>, <span>shipcountry</span>, <span>shipregion</span> и <span>shipcity</span>.",
                    Answer = "select id, orderdate, shipcountry, CASE WHEN shipregion is null THEN 'Not set' END as shipregion, shipcity"+Environment.NewLine +"from orders"+Environment.NewLine +"where shipregion IS null"
                },
                 new Task {
                    Id = 3,
                    LessonId = 1,
                    Number = 3,
                    TextAsk = "Выбрать в таблице <span>orders</span> заказы, которые сделаны после 27 апреля 2018 года (<span>orderdate</span>) не включая эту дату и которые еще не доставлены (<span>shippeddate</span> = null). В запросе должны возвращаться только колонки <span>order.id</span> (переименовать в Номер заказа), <span>requireddate</span> (переименовать в “Дата доставки”) и <span>shippeddate</span> (переименовать в “Дата отправки”). В результатах запроса возвращать для колонки “Дата отправки” вместо значений NULL строку ‘-’, для остальных значений возвращать дату в формате по умолчанию.",
                    Answer = "select id as \"Номер заказа\", requireddate as \"Дата доставки\", CASE WHEN shippeddate is NULL THEN '-' END as \"Дата отправки\""+Environment.NewLine +"from orders"+Environment.NewLine +"where orderdate > '2018-04-27' and shippeddate IS null"
                },
                      //les2
                    new Task {
                    Id =4 ,
                    LessonId = 2,
                    Number = 1,
                    TextAsk = "Выбрать из таблицы supplier всех поставщиков, расположенных в UK и France (колонка country). Запрос сделать с только помощью оператора IN. Возвращать companyname, contactname, city, phone. Упорядочить результаты запроса по country и companyname",
                    Answer = "select companyname, contactname, city, phone"+Environment.NewLine +"from suppliers"+Environment.NewLine +"where country in ('UK', 'France')"+Environment.NewLine +"order by country, companyname"
                },
                    new Task {
                    Id = 5,
                    LessonId = 2,
                    Number = 2,
                    TextAsk = "Выбрать из таблицы supplier всех поставщиков, не расположенных в UK и France (колонка country). Запрос сделать с только помощью оператора IN. Возвращать companyname, contactname, city, phone. Упорядочить результаты запроса по country по возрастанию и companyname по убыванию.",
                    Answer = "select companyname, contactname, city, phone"+Environment.NewLine +"from suppliers"+Environment.NewLine +"where country not in ('UK', 'France')"+Environment.NewLine +"order by country ASC, companyname DESC"
                },
                    new Task {
                    Id = 6,
                    LessonId = 2,
                    Number = 3,
                    TextAsk = "Выбрать из таблицы supplier все страны (колонка country), в которых размещены поставщики. В результате страна должна быть упомянута только один раз и список отсортирован по убыванию. Не использовать предложение GROUP BY. Возвращать только одну колонку в результатах запроса (колонка country).",
                    Answer = "select distinct country"+Environment.NewLine +"from suppliers"+Environment.NewLine +"order by country desc"
                },
                    //lesson3
                     new Task {
                    Id = 7,
                    LessonId = 3,
                    Number = 1,
                    TextAsk = "Выбрать все поставщиков (supplierid) из таблицы products (поставщики в результате не должны повторяться), где цена за единицу равна *10-19 *включительно – это колонка unitprice в таблице products.Использовать оператор BETWEEN. Запрос должен возвращать только колонку supplierid",
                    Answer = "select distinct supplierid"+Environment.NewLine +"from products"+Environment.NewLine +"where unitprice between 10 and 19 and unitprice != 19"
                },
                   new Task {
                    Id = 8,
                    LessonId = 3,
                    Number = 2,
                    TextAsk = "Выбрать всех заказы из таблицы orders, у которых заказчик (customerid) начинается на буквы из диапазона C и N. Использовать оператор BETWEEN. Запрос должен возвращать только колонки order.id и customerid , shipcountry и отсортирован по shipcountry.",
                    Answer = "select id, customerid, shipcountry"+Environment.NewLine +"from orders"+Environment.NewLine +"where substring(customerid, 1, 1) between 'C' and 'N'"+Environment.NewLine +"order by shipcountry"
                },
                    new Task {
                    Id = 9,
                    LessonId = 3,
                    Number = 3,
                    TextAsk = "Выбрать всех заказы из таблицы orders, у которых название страны доставки  (shipcountry) начинается на буквы из диапазона b и g, не используя оператор BETWEEN.",
                    Answer = "select *"+Environment.NewLine +"from orders"+Environment.NewLine +"where substring(shipcountry, 1, 1) >= 'b' and substring(shipcountry, 1, 1) <= 'g'"
                },
                    //lesson 4
                    new Task {
                    Id = 10,
                    LessonId = 4,
                    Number = 1,
                    TextAsk = "В таблице orders найти все заказы, где имя для доставки (shipname) начинается с Bo",
                    Answer = "select *"+Environment.NewLine +"from orders"+Environment.NewLine +"where shipname like 'Bo%'"
                },
                    //lesson 5
                    new Task {
                    Id = 11,
                    LessonId = 5,
                    Number = 1,
                    TextAsk = "Найти общую сумму всех заказов из таблицы <span>orderdetails</span> с учетом количества закупленных товаров и скидок по ним. Результатом запроса должна быть одна запись с одной колонкой с названием колонки '<span>Totals</span>'",
                    Answer = "select sum(cast (unitprice * quantity * (1 - discount) as real))"+Environment.NewLine +"from orderdetails"
                },
                    new Task {
                    Id = 12,
                    LessonId = 5,
                    Number = 2,
                    TextAsk = "По таблице <span>orders</span> найти количество заказов, которые еще не были доставлены (т.е. в колонке <span>shippeddate</span> нет значения даты доставки). Использовать при этом запросе только оператор COUNT. Не использовать предложения WHERE и GROUP.",
                    Answer = "select count(shippeddate)"+Environment.NewLine +"from orders"
                },
                    new Task {
                    Id = 13,
                    LessonId = 5,
                    Number = 3,
                    TextAsk = "По таблице <span>orders</span> найти количество различных покупателей (<span>customerid</span>), сделавших заказы. Использовать функцию COUNT и не использовать предложения WHERE и GROUP.",
                    Answer = "select count(distinct customerid)"+Environment.NewLine +"from orders"
                },
                    //lesson 6
                    new Task {
                    Id = 14,
                    LessonId = 6,
                    Number = 1,
                    TextAsk = "По таблице orders найти количество заказов с группировкой по годам. В результатах запроса надо возвращать две колонки c названиями Год и Количество.",
                    Answer = "select year(orderdate) as Год, count(*) as Количество"+Environment.NewLine +"from orders"+Environment.NewLine +"group by  year(orderdate) "
                },
                    new Task {
                    Id = 15,
                    LessonId = 6,
                    Number = 2,
                    TextAsk = "По таблице orders найти количество заказов, обслуженным каждым продавцом. Колонка employeeid является идентификатором. В результатах выполнения запроса нужно показать имя и фамилию продавца в одной колонке ( объединение lastname и firstname).  Для получения Имя/фамилия продавца НЕ использовать подзапрос. Основная группировка по employeeid .  В результате вывести две колонки: “Продавец” (объединение lastname и firstname) и колонку c количеством  заказов возвращать с названием “Заказов”. Результаты запроса должны быть упорядочены по убыванию количества заказов.",
                    Answer = "SELECT lastname + ' ' + firstname as Продавец , count(*) as Заказов"+Environment.NewLine +"FROM orders"+Environment.NewLine +"LEFT JOIN employees ON orders.employeeid = employees.id"+Environment.NewLine +"GROUP BY employees.id, lastname, firstname"+Environment.NewLine +"ORDER BY Заказов DESC"
                },
                    new Task {
                    Id = 16,
                    LessonId = 6,
                    Number = 3,
                    TextAsk = "По таблице orders найти количество заказов, выполненных каждым продавцом (employeeid)  и для каждого покупателя(customerid). Необходимо определить это только для заказов,  сделанных в 2017 году.",
                    Answer = "select employeeid as Продавец, customerid as Покупатель, COUNT(*) as \"Количество выполненных заказов\""+Environment.NewLine +"from orders"+Environment.NewLine +"where year(orderdate)=2017"+Environment.NewLine +"group by employeeid, customerid"
                },
                    new Task {
                    Id = 17,
                    LessonId = 6,
                    Number = 4,
                    TextAsk = "Найти всех покупателей, которые живут в одном городе. Показывать те города, в которых живет более 2 покупателя в одном городе.",
                    Answer = "select shipcity as city, count(distinct customerid) as customers"+Environment.NewLine +"from orders"+Environment.NewLine +"group by shipcity"+Environment.NewLine +"having  count(distinct customerid) > 2"
                },
                    new Task {
                    Id = 18,
                    LessonId = 6,
                    Number = 5,
                    TextAsk = "По таблице employees найти для каждого продавца его руководителя.",
                    Answer = "select lastname + ' ' + firstname as employeer, (select lastname + ' ' + firstname from employees where id = empl.reportsto) as leader"+Environment.NewLine +"from employees as empl"
                },
                    //lesson 7
                    new Task {
                    Id = 19,
                    LessonId = 7,
                    Number = 1,
                    TextAsk = "Определить продавцов, которые обслуживают регион 'Northern' (таблица region).",
                    Answer = "select DISTINCT lastname + ' ' + firstname, regiondescription"+Environment.NewLine +"from employees "+Environment.NewLine +"inner join employeeterritories on employees.id = employeeterritories.employeeid "+Environment.NewLine +"inner join territories on territories.id = employeeterritories.TerritoryId"+Environment.NewLine +"inner join regions on regions.id = territories.regionid"+Environment.NewLine +"where regiondescription = 'Northern'"
                },
                    new Task {
                    Id = 20,
                    LessonId = 7,
                    Number = 2,
                    TextAsk = "Выдать в результатах запроса имена всех заказчиков из таблицы customers и суммарное количество их заказов из таблицы orders. Принять во внимание, что у некоторых заказчиков нет заказов, но они также должны быть выведены в результатах запроса. Упорядочить результаты запроса по возрастанию количества заказов.",
                    Answer = "select customers.id, customers.companyname, COUNT(orders.id) as count"+Environment.NewLine +"from customers "+Environment.NewLine +"LEFT JOIN orders ON orders.customerid = customers.id"+Environment.NewLine +"group by customers.id, customers.companyname"+Environment.NewLine +"order by count"
                },
                    //lesson 8
                    new Task {
                    Id = 21,
                    LessonId = 8,
                    Number = 1,
                    TextAsk = "Выдать всех продавцов, которые имеют более 20 заказов. Использовать вложенный SELECT. В результате вывести firstname, lastname, region, country.",
                    Answer = "select firstname, lastname, region, country "+Environment.NewLine +"from employees "+Environment.NewLine +"where (select count(id) from orders where employees.id = orders.employeeid group by employeeid)>20"
                },
                    new Task {
                    Id = 22,
                    LessonId = 8,
                    Number = 2,
                    TextAsk = "Выдать всех заказчиков (таблица customers), которые не имеют ни одного заказа (подзапрос по таблице orders). Использовать оператор EXISTS. В результате вывести companyname, contactname, country.",
                    Answer = "select id, companyname, contactname, country "+Environment.NewLine +"from customers "+Environment.NewLine +"where NOT EXISTS( SELECT customerid from orders where customers.id = orders.customerid)"
                }

                });

            base.OnModelCreating(modelBuilder);
        }

    }
}
