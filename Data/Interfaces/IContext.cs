﻿using SQL.Data.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Interfaces
{
    public interface IContext
    {
        ApplicationDbContext Context { get; }
    }
}
