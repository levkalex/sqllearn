﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models
{
    public class EmployeeTerritory
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int TerritoryId { get; set; }
    }
}
