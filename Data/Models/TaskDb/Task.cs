﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.Models.TaskDb
{
    public class Task
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string TextAsk { get; set; }
        public string Answer { get; set; }

        public int? LessonId { get; set; }
        public Lesson Lesson { get; set; }
    }
}
