﻿using SQL.Data.Models.TaskDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = SQL.Data.Models.TaskDb.Task;

namespace SQL.Data.Interfaces
{
    public interface ITaskRepository
    {
        IQueryable<Task> Tasks { get; }
        IQueryable<Lesson> Lessons { get; }
    }
}
