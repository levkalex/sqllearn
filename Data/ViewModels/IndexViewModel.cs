﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SQL.Data.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<IEntityType> Types { get; set; }
        public int CountQuery { get; set; }
    }
}
